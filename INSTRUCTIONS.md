Problembeskrivning
==================

Du ska skriva ett konsol-program som tar som input en JSON-fil som (mycket förenklat) beskriver ett lån, och ger ifrån sig
en lista med de betalningar som ska göras. I Nordkap har vi baserat sättet vi hanterar sådant här på Discrete Event Simulation, så det kan vara en bra startpunkt.

Ett lån beskrivs med fyra typer av händelser. Alla händelser har ett datum då de infaller.

* Start, som alltid kommer först och anger lånets storlek och när det påbörjas.
* InterestFixing, som sätter räntesatsen från och med datumet.
* PaymentPeriodEnd, som anger när räntan för en viss betalning ska räknas samman.
* Invoice, som anger när betalningen ska ske och hör till en specifik PaymentPeriodEnd.

Händelser beskrivs med JSON-objekt i en array. Objekten kommer alltid i den ordning de infaller.

Tänk på att huvudsyftet med övningen är att vi ska ha något att prata om, inte att försöka lösa ett "riktigt" problem i verkligheten.

## JSON-format input

Alla händelser har en property `type` som anger vilken händelse det är, och `date` som anger datumet då händelsen infaller.

### Start

Det finns alltid exakt en start-händelse, och det kommer alltid först. Det har en property `amount` som anger beloppet ränta ska betalas på.

### InterestFixing

qDet finns alltid ett InterestFixing som infaller på samma datum som Start. Det kan också finnas fler, men aldrig flera på samma dag. Räntesatsen anges med en property `rate`, och gäller från och med den dagen händelsen infaller.

### PaymentPeriodEnd

En PaymentPeriodEnd anger att räntan sedan senaste PaymentPeriodEnd (eller Start, för den första) ska räknas ihop, för att betalas på motsvarande Invoice. Varje PaymentPeriodEnd har exakt en motsvarande Invoice. Ränta räknas från och med förra periodens slut till denna händelse. T.ex. om lånet startar 2019-03-01 och första PaymentPeriodEnd kommer 2019-04-01 så betalas ränta för hela mars, men ingen dag i april.

### Invoice

En Invoice anger att en betalning ska ske dagen händelsen inträffar. En property `periodEnding` anger en PaymentPeriodEnd vars ränta skall betalas. Det finns alltid exakt en Invoice som svarar mot varje PaymentPeriodEnd, men de behöver inte komma i samma ordning som de PaymentPeriodEnd de motsvarar. De kan komma innan motsvarande PaymentPeriodEnd, men ska ändå använda beloppet från framtiden.

## JSON-format output

Betalningarna ska komma i datumordning i en JSON-array, där varje betalning är ett objekt med properties `date` och `amount`.

## Ränteberäkning

Det finns många sätt att beräkna hur mycket som ska betalas givet en viss räntesats och en viss tidsperiod, men för det här programmet tänker vi oss vad som brukar kallas "Actual/365", dvs för varje kalenderdag ska man betala (räntesatsen * beloppet)/365. Detta gäller även skottår. T.ex., om beloppet är 10000 SEK och räntesatsen 3,65%, ska man betala 1 SEK för varje dag.

## Exempel-lån

Ett lån på 1 miljon som löper från 2019-03-01 till 2019-09-01, och har två räntebetalningstillfällen skulle kunna se ut så här:

```json
[
	{
		"type": "Start",
		"amount": 1000000,
		"date": "2019-03-01"
	},
	{
		"type": "InterestFixing",
		"rate": 0.32,
		"date": "2019-03-01"
	},
	{
		"type": "PaymentPeriodEnd",
		"date": "2019-06-01"
	},
	{
		"type": "Invoice",
		"periodEnding": "2019-06-01",
		"date": "2019-06-01"
	},
	{
		"type": "PaymentPeriodEnd",
		"date": "2019-09-01"
	},
	{
		"type": "Invoice",
		"periodEnding": "2019-06-01",
		"date": "2019-09-01"
	}
]
```

och skulle rendera betalningarna

```json
[
  {
    "date": "2019-06-01T00:00:00",
    "amount": 806.58
  },
  {
    "date": "2019-09-01T00:00:00",
    "amount": 806.58
  }
]
```

## Fler exempel

Det finns några till exempel på filer som skall kunna hanteras av ditt program i `examples/`, varav några är av enklare karaktär, och en fil som är komplicerad.