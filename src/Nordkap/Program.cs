﻿using System;
using System.IO;

using CommandLine;

using Microsoft.Extensions.DependencyInjection;

using Nordcap.ApplicationServices;

using Nordkap.ApplicationServices;

using Serilog;

namespace Nordkap
{
    public class Options
    {
        [Option('f', Required = true, HelpText = "Json file containing loan events")]
        public string File { get; set; }

        [Option('v', "verbose", Required = false, HelpText = "Set logging output to verbose messages.")]
        public bool Verbose { get; set; }
    }

    internal class Program
    {
        private static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                                  .AddTransient<IEventParser, EventParser>()
                                  .AddTransient<IJsonLoanCalculatorService, JsonLoanCalculatorService>()
                                  .BuildServiceProvider();

            Parser.Default.ParseArguments<Options>(args)
                  .WithParsed(async o =>

                  {
                      if (o.Verbose)
                      {
                          Log.Logger = new LoggerConfiguration()
                                       .MinimumLevel.Verbose()
                                       .WriteTo.Console().CreateLogger();
                      }

                      if (!string.IsNullOrEmpty(o.File) && File.Exists(o.File))
                      {
                          var service = serviceProvider.GetService<IJsonLoanCalculatorService>();
                          var data = await File.ReadAllTextAsync(o.File);
                          var s = service.GetLoanPaymentsAsJson(data);

                          Console.WriteLine(s);
                          Console.ReadKey();
                      }
                      else
                      {
                          Log.Error("Please specify a filename after the -f parameter");
                          Console.ReadKey();
                      }
                  });
        }
    }
}
