﻿using System;

namespace Nordkap.Domain.Interfaces
{
    public interface IEvent
    {
        DateTime Date { get; }
      
    }

}
