﻿namespace Nordkap.Domain.Interfaces
{
    public interface IInterestFixing : IEvent
    {
        decimal Rate { get;  }
    }
}