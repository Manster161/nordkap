﻿using System;

namespace Nordkap.Domain.Interfaces
{
    public interface IInvoice : IEvent
    {
        DateTime PeriodEnding { get; }
    }
}