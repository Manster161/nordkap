﻿using System;

namespace Nordkap.Domain.Interfaces
{
    public interface IStart : IEvent
    {
        DateTime Date { get; }
        decimal Amount { get; }
    }
}
