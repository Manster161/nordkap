﻿using System;

using Nordkap.Domain.Interfaces;

namespace Nordkap.Domain.Entities
{
    public class Start : Event, IStart
    {
        public decimal Amount { get; }

        public Start(decimal amount, DateTime date)
        {
            Date = date;
            if (amount <= 0)
            {
                throw new ArgumentException("Amount cannot be less or equal to zero");
            }
            Amount = amount;
        }

        
    }
}