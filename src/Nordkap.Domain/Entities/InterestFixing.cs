﻿using System;

using Nordkap.Domain.Interfaces;

namespace Nordkap.Domain.Entities
{
    public class InterestFixing : Event, IInterestFixing
    {
        public decimal Rate { get; }

        public InterestFixing(decimal rate, DateTime date)
        {
            Date = date;
            Rate = rate;
        }
    }
}