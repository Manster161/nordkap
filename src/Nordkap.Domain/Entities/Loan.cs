﻿using System;
using System.Collections.Generic;
using System.Linq;

using Nordkap.Domain.Interfaces;

using Serilog;

namespace Nordkap.Domain.Entities
{
    public class Loan
    {
        private readonly List<IEvent> _transactions;

        public DateTime Start
        {
            get
            {
                var d = (IStart)_transactions.First(t => t is IStart);
                return d.Date;
            }
        }

        public decimal Amount
        {
            get
            {
                var d = (IStart)_transactions.First(t => t is IStart);
                return d.Amount;
            }
        }

        public IReadOnlyList<IInterestFixing> InterestFixings
        {
            get { return _transactions.Where(t => t is IInterestFixing)?.Select(t => (IInterestFixing)t).ToList(); }
        }

        public IReadOnlyList<IPaymentPeriodEnd> PaymentPeriodEndings
        {
            get { return _transactions.Where(t => t is IPaymentPeriodEnd)?.Select(t => (IPaymentPeriodEnd)t).ToList(); }
        }

        public IReadOnlyList<IInvoice> Invoices
        {
            get { return _transactions.Where(t => t is IInvoice)?.Select(t => (IInvoice)t).ToList(); }
        }

        public Loan(IStart start)
        {
            _transactions = new List<IEvent> { start };
        }

        public void AddInterestFixing(IInterestFixing interestFixing)
        {
            if (InterestFixings.Count == 0 && interestFixing.Date.Date.Equals(Start))
            {
                _transactions.Add(interestFixing);
            }
            else if (InterestFixings.All(i => i.Date != interestFixing.Date))
            {
                _transactions.Add(interestFixing);
            }
            else
            {
                throw new ArgumentException();
            }
        }

        public void AddInvoice(IInvoice invoice)
        {
            if (Invoices.Any(i => i.PeriodEnding.Equals(invoice.PeriodEnding)))
            {
                throw new ArgumentException("Cannot add invoice when invoice for payment period already exists");
            }

            _transactions.Add(invoice);
        }

        public void AddPaymentPeriodEnd(IPaymentPeriodEnd paymentPeriodEnd)
        {
            if (PaymentPeriodEndings.Any(p => p.Date.Date.Equals(paymentPeriodEnd.Date)))
            {
                throw new ArgumentException("Cannot have two payment period dates on the same date");
            }

            _transactions.Add(paymentPeriodEnd);
        }

        public IReadOnlyList<Payment> GetPayments()
        {
            var payments = new List<Payment>();
            foreach (var invoice in Invoices)
            {
                decimal cost = 0;
                var endDate = GetInvoiceEndDate(invoice);
                var startDate = GetInvoiceStartDate(invoice);
                var rates = GetInvoiceRates(startDate, endDate);

                foreach (var rate in rates)
                {
                    var ratePeriodBeginning = GetRatePeriodBeginning(startDate, rate);
                    var ratePeriodEnding = GetRatePeriodEnding(endDate, rates, rate);

                    var costRatePeriod = Amount * (ratePeriodEnding - ratePeriodBeginning).Days * rate.Rate / 365m;
                    cost += costRatePeriod;

                    Log.Verbose($"Interval: {ratePeriodBeginning} - {ratePeriodEnding}");
                    Log.Verbose($"Days: {(ratePeriodEnding - ratePeriodBeginning).Days} with rate: {rate.Rate}");
                    Log.Verbose($"Cost rate period: {costRatePeriod}");
                }

                Log.Verbose($"Invoice: {cost}");
                Log.Verbose("");
                payments.Add(new Payment(invoice.Date, cost));
            }

            return payments;
        }

        private DateTime GetInvoiceEndDate(IInvoice invoice)
        {
            return PaymentPeriodEndings.First(p => p.Date == invoice.PeriodEnding).Date;
        }

        private List<IInterestFixing> GetInvoiceRates(DateTime startDate, DateTime endDate)
        {
            var rates = new List<IInterestFixing> { InterestFixings.Last(r => r.Date <= startDate) };
            var rateChanges = InterestFixings.Where(f => f.Date > startDate && f.Date < endDate).ToList();
            rates.AddRange(rateChanges);
            return rates;
        }

        private DateTime GetInvoiceStartDate(IInvoice invoice)
        {
            return PaymentPeriodEndings.LastOrDefault(p => p.Date < invoice.PeriodEnding)?.Date ?? Start.Date;
        }

        private static DateTime GetRatePeriodBeginning(DateTime startDate, IInterestFixing rate)
        {
            return startDate > rate.Date ? startDate : rate.Date;
        }

        private static DateTime GetRatePeriodEnding(DateTime endDate, List<IInterestFixing> rates, IInterestFixing rate)
        {
            return rate == rates.Last() ? endDate : rates[rates.IndexOf(rate) + 1].Date;
        }
    }
}
