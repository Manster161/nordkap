﻿namespace Nordkap.Domain.Entities
{
    public enum EventType   
    {
        Start = 1,
        InterestFixing,
        PaymentPeriodEnd,
        Invoice
    }
}
