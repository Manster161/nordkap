﻿using System;

using Nordkap.Domain.Interfaces;

namespace Nordkap.Domain.Entities
{
    public class Event : IEvent
    {  
        public DateTime Date { get; set; }
    }
}