﻿using System;

using Nordkap.Domain.Interfaces;

namespace Nordkap.Domain.Entities
{
    public class PaymentPeriodEnd : Event, IPaymentPeriodEnd
    {
        public PaymentPeriodEnd(DateTime date)
        {
            Date = date;
        }
    }
}
