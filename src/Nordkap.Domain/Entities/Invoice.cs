﻿using System;

using Nordkap.Domain.Interfaces;

namespace Nordkap.Domain.Entities
{
    public class Invoice : Event, IInvoice
    {
        public Invoice(DateTime date, DateTime periodEnding)
        {
            Date = date;
            PeriodEnding = periodEnding;
        }

        public DateTime PeriodEnding { get; }
    }
}
