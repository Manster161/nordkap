﻿using System;

namespace Nordkap.Domain.Entities
{
    public class Payment
    {
        private readonly decimal _amount;
        public decimal Amount => Math.Round(_amount, 2, MidpointRounding.AwayFromZero);
        public DateTime Date { get; }

        public Payment(DateTime date, decimal amount)
        {
            Date = date;
            _amount = amount;
        }
    }
}
