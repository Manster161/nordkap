﻿using System;
using System.Globalization;

namespace Nordkap.Utils
{
    public static class EnumUtil
    {
        /// <summary>
        ///     Parse string to en enum
        /// </summary>
        /// <typeparam name="TTypeTo"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static TTypeTo ParseEnum<TTypeTo>(string value)
        {
            return (TTypeTo)Enum.Parse(typeof(TTypeTo), value, true);
        }

        /// <summary>
        ///     Parse en integer to enum
        /// </summary>
        /// <typeparam name="TTypeTo"></typeparam>
        /// <param name="value"></param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static TTypeTo ParseEnum<TTypeTo>(int value)
        {
            return (TTypeTo)Enum.Parse(typeof(TTypeTo), value.ToString(CultureInfo.InvariantCulture));
        }

        /// <summary>
        ///     Converts between two enum types.
        /// </summary>
        /// <typeparam name="TTypeTo">Type to convert to</typeparam>
        /// <typeparam name="TTypeFrom">Type to convert from</typeparam>
        /// <param name="value">Enum value or name to convert</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static TTypeTo ParseEnum<TTypeFrom, TTypeTo>(TTypeFrom value)
        {
            return (TTypeTo)Enum.Parse(typeof(TTypeTo), value.ToString(), true);
        }

        /// <summary>
        ///     Convert value between enum types.
        ///     Sets value to default if enum value is not found in destination type.
        /// </summary>
        /// <typeparam name="TTypeTo">Type to convert to</typeparam>
        /// <typeparam name="TTypeFrom">Type to convert from</typeparam>
        /// <param name="value">Enum value or name to convert</param>
        /// <param name="defaultValue">Default value if conversion fails</param>
        /// <returns></returns>
        /// <remarks></remarks>
        public static TTypeTo ParseEnum<TTypeFrom, TTypeTo>(TTypeFrom value, TTypeTo defaultValue)
        {
            TTypeTo ret;

            try
            {
                ret = ParseEnum<TTypeFrom, TTypeTo>(value);
            }
            catch (ArgumentException)
            {
                ret = defaultValue;
            }

            return ret;
        }
    }
}

