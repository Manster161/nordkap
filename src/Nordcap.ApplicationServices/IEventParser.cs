﻿using Nordkap.Domain.Entities;

namespace Nordcap.ApplicationServices
{
    public interface IEventParser
    {
        Loan ParseLoan(string events);
    }
}