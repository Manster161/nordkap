﻿using System;

using Nordkap.Domain;
using Nordkap.Domain.Entities;

namespace Nordkap.ApplicationServices
{
    public class GenericTransactionWrapper
    {
        public DateTime Date { get;  set; }
        public EventType Type { get;  set; }
        public DateTime PeriodEnding { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }
    }
}