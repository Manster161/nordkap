﻿using System;
using System.Collections.Generic;
using System.Linq;

using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

using Nordkap.ApplicationServices;
using Nordkap.Domain;
using Nordkap.Domain.Entities;

namespace Nordcap.ApplicationServices
{
    public class EventParser : IEventParser
    {
        public Loan ParseLoan(string events)
        {
            var transactions = JsonConvert.DeserializeObject<IEnumerable<GenericTransactionWrapper>>(events).ToList();
            Loan loan = null;

            foreach (var genericTransactionWrapper in transactions)
            {
                switch (genericTransactionWrapper.Type)
                {
                    case EventType.Start:
                        loan = new Loan(new Start(genericTransactionWrapper.Amount, genericTransactionWrapper.Date));
                        break;
                    case EventType.InterestFixing:
                        loan.AddInterestFixing(new InterestFixing(genericTransactionWrapper.Rate, genericTransactionWrapper.Date));
                        break;
                    case EventType.Invoice:
                        loan.AddInvoice(new Invoice(genericTransactionWrapper.Date, genericTransactionWrapper.PeriodEnding));
                        break;
                    case EventType.PaymentPeriodEnd:
                        loan.AddPaymentPeriodEnd(new PaymentPeriodEnd(genericTransactionWrapper.Date));
                        break;
                    default:
                        throw new ArgumentOutOfRangeException($"Unknown evnt type: {genericTransactionWrapper.Type}");
                }
            }

            return loan;
        }
    }
}
