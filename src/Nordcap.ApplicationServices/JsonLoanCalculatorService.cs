﻿using System;
using System.IO;
using System.Text;

using Newtonsoft.Json;

using Nordcap.ApplicationServices;

namespace Nordkap.ApplicationServices
{
    public class JsonLoanCalculatorService : IJsonLoanCalculatorService
    {
        private readonly IEventParser _parser;

        public JsonLoanCalculatorService(IEventParser parser)
        {
            _parser = parser ?? throw new ArgumentException("Event parser is null");
        }

        public string GetLoanPaymentsAsJson(string jsonData)
        {
            var loan = _parser.ParseLoan(jsonData);

            return JsonConvert.SerializeObject(loan.GetPayments(), Formatting.Indented);
        }
    }
}
