﻿namespace Nordkap.ApplicationServices
{
    public interface IJsonLoanCalculatorService
    {
        string GetLoanPaymentsAsJson(string jsonData);
    }
}