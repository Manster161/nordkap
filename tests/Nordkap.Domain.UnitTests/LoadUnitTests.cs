﻿using System;
using Xunit;
using FluentAssertions;

using Nordkap.Domain.Entities;

namespace Nordkap.Domain.UnitTests
{
    public class LoadUnitTests
    {

        protected decimal Amount = 1000m;
        protected DateTime Date = DateTime.UtcNow.Date;
        protected decimal Rate = 0.4m;

        public class AddInterestFixing : LoadUnitTests
        {
            [Fact]
            public void When_adding_the_first_interest_fixing_it_has_to_have_the_same_start_date()
            {
                
                var loan = new Loan(new Start(Amount, Date));
                loan.AddInterestFixing(new InterestFixing(Rate, Date));

            }
            [Fact]
            public void When_interest_rate_with_different_date_then_it_cannot_be_the_first()
            {
                
                var loan = new Loan(new Start(Amount, Date));
                loan.AddInterestFixing(new InterestFixing(Rate, Date));
                loan.AddInterestFixing(new InterestFixing(Rate, Date.AddDays(1)));

                loan.InterestFixings.Count.Should().Be(2);
            }
            [Fact]
            public void When_adding_interest_fixing_it_cannot_exist_one_the_same_day()
            {
                var loan = new Loan(new Start(Amount, Date));
                loan.AddInterestFixing(new InterestFixing(Rate, Date));
                Action a = () => loan.AddInterestFixing(new InterestFixing(Rate, Date));

                a.Should().Throw<ArgumentException>();
            }
        }

        public class AddPaymentPeriodEnd : LoadUnitTests
        {
            [Fact]
            public void When_adding_a_payment_period_end_it_cannot_exist_one_with_the_same_date()
            {
                var loan = new Loan(new Start(Amount, Date));
                loan.AddInterestFixing(new InterestFixing(Rate, Date));
                loan.AddPaymentPeriodEnd(new PaymentPeriodEnd(Date.AddDays(30)));
                Action a = () => loan.AddPaymentPeriodEnd(new PaymentPeriodEnd(Date.AddDays(30)));

                a.Should().Throw<ArgumentException>();
            }
        }

        public class AddInvoice : LoadUnitTests
        {
            [Fact]
            public void When_adding_an_invoice_it_corresponds_to_one_PaymentPeriodEnd()
            {
                var loan = new Loan(new Start(Amount, Date));
                var paymentPeriodEndDate = Date.AddDays(30);
                loan.AddInterestFixing(new InterestFixing(Rate, Date));
                loan.AddPaymentPeriodEnd(new PaymentPeriodEnd(paymentPeriodEndDate));

                loan.AddInvoice(new Invoice(Date.AddDays(25), paymentPeriodEndDate));
            }

            [Fact]
            public void When_adding_an_invoice_it_corresponds_to_one_PaymentPeriodEnd_and_no_invoice_for_that_date_should_exist()
            {
                var loan = new Loan(new Start(Amount, Date));
                var paymentPeriodEndDate = Date.AddDays(30);
                loan.AddInterestFixing(new InterestFixing(Rate, Date));
                loan.AddPaymentPeriodEnd(new PaymentPeriodEnd(paymentPeriodEndDate));

                loan.AddInvoice(new Invoice(Date.AddDays(25), paymentPeriodEndDate));
                Action a = () => loan.AddInvoice(new Invoice(Date.AddDays(30), paymentPeriodEndDate));

                a.Should().Throw<ArgumentException>()
                    .WithMessage("Cannot add invoice when invoice for payment period already exists");
            }
        }
    }
}
